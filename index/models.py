from django.db import models
class Year(models.Model):
    year=models.IntegerField()
    def __str__(self):
        return (str(self.year))
class Friend(models.Model):
    name=models.CharField(max_length=30)
    hobby=models.CharField(max_length=30)
    fav_food=models.CharField(max_length=30)
    class_year= models.ForeignKey(Year, on_delete= models.CASCADE)
    def __str__(self):
        return self.name


# Create your models here.
