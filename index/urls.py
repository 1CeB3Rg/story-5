from django.urls import path
from . import views

urlpatterns = [
    path('',views.home,name='index-home'),
    path('memes/',views.memes,name='index-memes'),
    path('show/',views.friends,name="index-show"),
    path('add/',views.add,name='index-add')
]
