from django.shortcuts import render
from .models import Friend,Year
from .forms import FriendForm
def home(request):
    return render(request,'index/story_3.html')
def memes(request):
    return render(request,'index/meme.html')
def friends(request):
    friends= Friend.objects.all()
    return render(request,"index/show.html",{"friends":friends})
def add(request):
    return render(request,'index/Add.html')
def new_friend(request):
    if request.method == 'POST':
        form = FriendForm(request.POST)
        if form.is_valid():
            form.save()
            friends= Friend.objects.all()
            return render(request,"index/show.html",{"friends":friends})
        else:
            form= FriendForm()
        return render(request,'index/add.html',{'form':form})
